const { MindConnectAgent } = require ("@mindconnect/mindconnect-nodejs")
const fs = require("fs")
const mv = require("mv")

class IotAgent {
    // agent
    // config

    constructor(configuration) {
        this.agent = new MindConnectAgent(configuration.onboardingKey)
        this.config = configuration
    }

    init() {
        return this.onboard()
        .then(() => this.setupDataSource(this.config.id))
    }
    
    onboard() {
        if (!this.agent.IsOnBoarded()) {
            console.log('Onboarding agent...')
            return this.agent.OnBoard()
            .then(() => console.log("Onboparding complete."))
        } else {
            return Promise.resolve()
        }
    }

    setupDataSource(assetId) {
        if(!this.agent.HasDataMappings()) {
            console.log(`Configuring agent for asset with ID ${assetId}...`)
            return this.agent.ConfigureAgentForAssetId(assetId)
            .then(() => console.log('Configuration complete.'))
        } else {
            return Promise.resolve()
        }
        
    }

    sendJson(data) {
        if(!Array.isArray(data)) {
            data = [data]
        }

        const mindsphereData = data.map(entry => ({
            "timestamp": new Date().toISOString(),
            "values": Object.entries(entry).map(([key, value]) => ({
                "dataPointId": `DP-${key.replace('-', '_')}`,
                "value": value,
                "qualityCode": "1"
            }))
        }))

        console.log('Sending data points...');
        return this.agent.BulkPostData(mindsphereData)
        .then(() => console.log('Data points sent.'))
    }

    moveFile(inFile, outFile) {
        return new Promise((resolve, reject) => {
            mv(inFile, outFile, {mkdirp: true}, function(err) {
                if(err) {
                    reject(err)
                } else {
                    resolve()
                }
            })
        })
    }

    processFiles() {
        // Create in dir if needed:
        const inPath = `./in/${this.config.name}`
        if(!fs.existsSync(inPath)) {
            fs.mkdirSync(inPath, { recursive: true })
            return // in dir didn't exist, so nothing to scan yet
        }

        // Create processing dir if needed:
        const processingPath = `./processing/${this.config.name}`
        if(!fs.existsSync(processingPath)) {
            fs.mkdirSync(processingPath, { recursive: true })
        }

        // For each incoming file for this agent convert to JSON and send:
        const outPath = `./out/${this.config.name}`
        return fs.promises.readdir(inPath)
        .then(files => Promise.all(files.map(file => {
            let promises = []

            // Map JSON data and upload:
            if(this.config.dataMap) {
                promises.push(
                    fs.promises.readFile(`${inPath}/${file}`, "utf8")
                    .then(data => this.config.dataMap(data))
                    .then(json => this.sendJson(json))
                )
            }

            // Upload raw file data:
            if(this.config.fileMap) {
                promises.push(
                    fs.promises.readFile(`${inPath}/${file}`, "utf8")
                    .then(data => this.config.fileMap(data))
                    .then(data => {
                        if(typeof data.data === 'object') {
                            data.data = JSON.stringify(data)
                        }
                        console.log(`Sending file ${processingPath}/${file}...`)
                        return fs.promises.writeFile(`${processingPath}/${file}`, data)
                        .then(() => data.filename)
                    })
                    .then(filename => this.agent.UploadFile(this.agent.ClientId(), filename, `${processingPath}/${file}`, {
                        retry: 3,
                        description: file + " uploaded on " + new Date(),
                        parallelUploads: 5,
                        chunk: true
                    }))
                    .then(() => console.log('File sent.'))
                    .then(() => fs.promises.unlink(`${processingPath}/${file}`)) // delete processing file
                )
            }

            return Promise.all(promises)
            .then(() => this.moveFile(`${inPath}/${file}`, `${outPath}/${file}`))
        })))
    }
}

// Periodically scan incoming directories:
let agents
let scanInterval
function initAgents(config) {
    scanInterval = config.scanInterval
    agents = config.assets.map(asset => new IotAgent(asset));
    return Promise.all(agents.map(agent => agent.init()))
}

let scanEnabled = false
function startScanning() {
    scanEnabled = true
    scanFiles()
}

function stopScanning() {
    scanEnabled = false
}

function scanFiles() {
    if(!scanEnabled) {
        return
    }
    console.log(`Scanning '${fs.realpathSync('./')}/in' for new files...`)
    Promise.all(agents.map(agent => agent.processFiles()))
    .then(() => console.log('Scan complete.'))
    .then(() => setTimeout(scanFiles, scanInterval)) // Schedule next scan
    .catch(console.error)
}

module.exports = {
    initAgents,
    startScanning,
    stopScanning
}
