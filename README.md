# ellipsis-mindconnect

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

> Provides a command for scanning an incoming directory for files, processing them and sending data to Mindasphere.

This script manages the interpretation and flow of data files to be sent to Mindsphere.
Assets are configured such that incoming directories are watched, and any new files coming in are processed.
Incoming files are placed in the ./in/assetname directory, processed files are automatically moved to the ./out/assetname directory.
Configuration of a new asset required addition of an asset to the `assets.json` file and the addition of a corresponding mapping function to the `config.js` file.
The script is designed to be run as a background process.

## Table of Contents

- [ellipsis-mindconnect](#ellipsis-mindconnect)
  - [Table of Contents](#table-of-contents)
  - [Install](#install)
  - [Usage](#usage)
    - [Configuration](#configuration)
    - [Asset Setup](#asset-setup)
    - [Asset Creation in Mindsphere](#asset-creation-in-mindsphere)
    - [Asset Onboarding](#asset-onboarding)
  - [Maintainers](#maintainers)
  - [Contributing](#contributing)
  - [License](#license)

## Install

```
git clone https://bitbucket.org/ellipsistechnology/ellipsis-mindconnect.git
```

## Usage

```
npm run start
```

### Configuration

Configuration is specified in the `config.json` file.

|Key|Description|
 |---|---|
 |`scanInterval`||
 |`dataMaps`|A map of key-value pairs where the key is the name of the asset and the value is a function that takes a string as its only parameter and returns a JSON object.|

### Asset Setup

Assets are specified in the `assets.json` file which containts an array of assets.
Each asset must include a name and id, and if not yet onboarded will require an onboardingKey from the Mindsphere Agent.
See [Asset Creation in Mindsphere](#asset-creation-in-mindsphere) below for details on how to create the asset and agent in Mindsphere.
See [Asset Onboarding](#asset-onboarding) below for details on how to onboard an asset via an Agent in Mindsphere.

Asset structure:

|Key|Description|
 |---|---|
 |`name`|The name of the asset. This must match the name of the data map in [`config.js`](#configuration).|
 |`id`|The id of the Asset as specified in Mindasphere.|
 |`isConfigured`|A boolean value indicating if the asset/agent data mapping is complete. If false, the first time agent is used the mapping will be automatically generated.|
 |`onboardingKey`|The onboarding key of the agent found in Mindsphere.|

Example `assets.json` file:

```
[
  {
    "name": "etongue",
    "id": "86f361a751e249aaa893009981b403fa",
    "isConfigured": true,
    "onboardingKey": {
      "content": {
        "baseUrl": "https://southgate.eu1.mindsphere.io",
        "iat": "eyJraWQiOiJrZXktaWQtMSIsInR5cCI6IkpXVCIsImFsZyI6IlJTMjU2In0.eyJpc3MiOiJTQ0kiLCJzdWIiOiIyYTlhYjgxMTVkM2M0MGViOTg3NzZkMzFmMjA0NTMxZSIsImF1ZCI6IkFJQU0iLCJpYXQiOjE2MDIyMzU3ODQsIm5iZiI6MTYwMjIzNTc4NCwiZXhwIjoxNjAyODQwNTg0LCJqdGkiOiJiOWVmMzI0ZS02NjkyLTQwNTMtODQwNS02MTg1MTUyNzliMTYiLCJzY29wZSI6IklBVCIsInRlbiI6InV0YXNpb3QiLCJ0ZW5fY3R4IjoibWFpbi10ZW5hbnQiLCJjbGllbnRfY3JlZGVudGlhbHNfcHJvZmlsZSI6WyJTSEFSRURfU0VDUkVUIl0sInNjaGVtYXMiOlsidXJuOnNpZW1lbnM6bWluZHNwaGVyZTp2MSJdfQ.bcD8BpFg0wfQozyHmAfJTq8Na6gwJaxKH1C-r3KDwgktp1nGPcqbGK_AQ-65lzr9Wy3BPhusJte4Ae1TbD362Yb80GsPUI-OCidWmZInQ9JlJoRNjLWCrdBfuuTALCagrgepY0M29coUM4CrfBsAKELv0lncD9C9-UR4axumbnM20RloLxwY4e989bk_Emz6w3YCxaCEDT2niO0q8i-ZrWI1u8VpFcyQQ-QqS84M8BcZnE4CxZNLbt-9sZPEbEcgO0DUl6wrZcHQYbOOCrwRiOsYzev3FtoP4KHGRev482V4MIUBQeEMaOQseVmnwAIZATf5bZnFLyP3FNnVLED3cg",
        "clientCredentialProfile": [
          "SHARED_SECRET"
        ],
        "clientId": "2a9ab8115d3c40eb98776d31f204531e",
        "tenant": "utasiot"
      },
      "expiration": "2020-10-16T09:29:44.000Z"
    }
  }
]
```

### Asset Creation in Mindsphere

1. Log into Mindsphere and open the AssetManager
2. Create an aspect:
    * Select Aspects from the left menu.
    * Create a new aspect.
    * Add you variables.
3. Create a Type:
    * Select types from left menu.
    * Create a new type.
    * Select an aspect - make sure you use the dynamic category in order to send time series data to the agent.
4. Create an asset:
    * Select assets from the left menu.
    * If you want to group your assets create an asset and select the Area asset type to use as a hierarchy node and navigate into this new area by clicking on the right arrow in a circle next to it after you've created it.
    * Create an asset and select the type you created above.
    * Find the asset id and copy it into the `id` field of the asset in the `assets` array in the `initAgents()` configuration parameter (you can find the asset's ID in the URL, look for the `selected` query parameter).
5. Create an agent:
    * Select assets from the left menu (if you're not already there).
    * Create a new child asset (select the circled + icon in the top-right).
    * Select MindConnectLib.
    * Fill in the details for name etc. and save.
    * Select the new agent and click on the MindConnectLib arrow down the bottom.
    * Select the security you want (shared secret is easier, RSA_3072 is more secure).

### Asset Onboarding

Agents will be automatically onboarded (if not already onboarded) when the script runs. This requires that the initial access token (IAT) be created and copied into the asset configuration as follows:

1. Select assets in the left menu.
2. Select the new agent and click on the MindConnectLib arrow down the bottom.
3. Click the gear button and press Generate onboarding key.
4. Copy the entire JSON object and paste it into the `onboardingKey` field of the asset in the `assets` array in the `initAgents()` configuration parameter.

## Maintainers

[@ellipsistechnology](https://bitbucket.org/ellipsistechnology)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2020 Ellipsis Technology
